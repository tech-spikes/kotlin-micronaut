object Version {
    const val commonsLogging = "1.2"
    const val jackson = "2.9.8"
    const val kotlin = "1.3.21"
    const val logback = "1.2.3"
    const val micronaut = "1.0.4"
    const val micronautSpring = "1.0.0.M3"
    const val rxJdbc = "0.2.2"
    const val postgres = "42.2.5"

    object Test {
        const val kotlintest = "3.2.1"
        const val mockk = "1.9"
        const val micronautTest = "1.0.2"
        const val testContainers = "1.10.6"
    }

    object Build {
        const val dependencyManagement = "1.0.6.RELEASE"
        const val shadow = "4.0.4"
    }

    object Tool {
        const val detekt = "1.0.0-RC13"
    }
}
