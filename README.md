# Example Micronaut app (with Spring MVC integration)

This project contains a Hello World Micronaut application. It is configured with Spring integration, but most of the examples are pure Micronaut controllers.

Contains the following examples:

* Hello World Spring controller: `‌DeceptiveController`
* Hello World Micronaut controller: `HonestController`
* URL templates and query parameters: `AdvancedController`
* Request validation: `ValidatedController`
* Reactive (RxJava2) support: `ReactiveController`
* Reactive data access ([rxjava2-jdbc](https://github.com/davidmoten/rxjava2-jdbc)): `CustomerRepository`, `CustomerController`
* [Micronaut operations](https://docs.micronaut.io/latest/guide/index.html#clientAnnotation) (shared interface between controller and client): `GreetingOperations`, `GreetingController`, `GreetingClient`
* [OpenAPI integration](https://docs.micronaut.io/latest/guide/index.html#openapi):
    * Micronaut is capable of generating OpenAPI/Swagger specification for your controllers
    * Out of the box it doesn't come with Swagger UI support, but you can easily package Swagger UI yourself
    * This project packages [Swagger UI](https://github.com/swagger-api/swagger-ui/tree/master/dist) and has configuration for exposing it as static resource in `application.yml`
    * Swagger UI is available at [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)
* Integration with [Kotlintest and BDD](https://github.com/kotlintest/kotlintest/blob/master/doc/styles.md#behavior-spec):
    * low-level tests: `AdvancedControllerSpec`
    * high-level tests: `GreetingControllerSpec`
* End-to-end testing and integration with [Testcontainers](https://www.testcontainers.org/) (the test spins up a Postgres database in a Docker container for the duration of the test run, so you don't have to mock the data access layer): `CustomerControllerSpec` - After the container image is downloaded, the test runs really fast! ⚡️

## Setup

You have to run Docker for E2E testing to work.

## Build & Run

You can run the application as a standard Micronaut application with `./gradlew run`. You can run tests with `./gradlew test`.