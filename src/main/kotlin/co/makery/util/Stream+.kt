package co.makery.util

import java.util.stream.Stream

fun <T> Stream<T>.iterable(): Iterable<T> = object : Iterable<T> {
    override fun iterator(): Iterator<T> = this@iterable.iterator()
}
