package co.makery

import io.micronaut.runtime.Micronaut
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info

@OpenAPIDefinition(
    info = Info(
        title = "Kotlin Micronaut",
        version = "0.1.0",
        description = "Kotlin Micronaut APIs"
    )
)
object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .mainClass(Application.javaClass)
            .start()
    }
}
