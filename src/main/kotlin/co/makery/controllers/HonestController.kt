package co.makery.controllers

import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Status

@Controller("/honest")
class HonestController {

    @Get("/who-am-i")
    @Status(HttpStatus.OK)
    fun whoAmI(): String = "A Micronaut controller"

    // http localhost:8080/honest/who-am-i
}
