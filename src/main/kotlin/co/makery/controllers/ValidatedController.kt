package co.makery.controllers

import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Controller("/validated")
@Validated
class ValidatedController {

    @Post("/register")
    fun register(@Valid @Body customer: Customer): RegistrationResponse =
        RegistrationResponse("Welcome ${customer.name}!", customer)

    // http -v localhost:8080/validated/register name=Maker
    // http -v localhost:8080/validated/register name=Maker email=you age=30
    // http -v localhost:8080/validated/register name=Maker email=you
    // http -v localhost:8080/validated/register name=Maker email=you@makery.co
}

data class Customer(
    val name: String,
    @get:Email val email: String,
    // val email: String?,
    @get:Min(18) @get:Max(99) val age: Int
    // val age: Int
)

data class RegistrationResponse(
    val message: String,
    val customer: Customer
)
