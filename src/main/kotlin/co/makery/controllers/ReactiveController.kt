package co.makery.controllers

import co.makery.util.iterable
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.FileReader

@Controller("/reactive")
class ReactiveController {

    @Get("/single")
    fun single(): Single<String> = Single.just("Hello Rx!")

    // http localhost:8080/reactive/single

    @Get(uri = "/stream", produces = [MediaType.APPLICATION_JSON_STREAM])
    fun stream(): Flowable<String> =
        Flowable
            .using(
                // this file is ~350MBs and contains line separated JSON entries from the GitHub archive
                { FileReader("<path to GitHub archive>/archive-000000000000.json").buffered() },
                { reader -> Flowable.fromIterable<String>(reader.lines().iterable()) },
                { reader -> reader.close() }
            )
            //.take(3L)

    // http localhost:8080/reactive/stream
    // wrk -t4 -c25 -d15s http://localhost:8080/reactive/stream
}
