package co.makery.controllers

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue

@Controller("/advanced")
class AdvancedController {

    @Get("/html")
    @Produces(MediaType.TEXT_HTML)
    fun html(): String = """
        <html>
            <title>
                <h1>HTML</h1>
            </title>
            <body></body>
        </html>""".trimIndent()

    // http localhost:8080/advanced/html

    @Get("/greet/{name}")
    fun greet(name: String): String = "Hello $name!"

    // http localhost:8080/advanced/greet/Makers

    @Get("/greet-opt{/name}")
    fun greetOpt(name: String?): String = "Hello $name!"

    // http localhost:8080/advanced/greet-opt/

    @Get("/greet-query")
    fun greetQuery(@QueryValue name: String): String = "Hello $name!"

    // http -v localhost:8080/advanced/greet-query name==Makers
    // http -v localhost:8080/advanced/greet-query

    @Post("/greet-json")
    fun greetJson(@Body request: Map<String, String>): Greeting = Greeting("Hello ${request["name"]}!")

    // http -v localhost:8080/advanced/greet-json name=Makers
}

data class Greeting(val message: String)
