package co.makery.controllers

import co.makery.repository.CustomerRepository
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Flowable
import javax.inject.Inject

@Controller("/customers")
class CustomerController {

    @Inject
    private lateinit var repository: CustomerRepository

    @Get
    fun customers(): Flowable<Customer> = repository.fetchCustomers()

    // http localhost:8080/customers
}
