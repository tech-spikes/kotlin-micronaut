package co.makery.controllers

import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client
import io.micronaut.retry.annotation.Fallback
import io.micronaut.validation.Validated
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class GreetingRequest(@get:NotBlank val name: String)

data class GreetingResponse(val message: String)

@Validated
interface GreetingOperations {

    @Post("/greet")
    fun greet(@Valid @Body request: GreetingRequest): GreetingResponse
}

@Controller("/greeting")
class GreetingController : GreetingOperations {

    override fun greet(request: GreetingRequest): GreetingResponse = GreetingResponse("Welcome ${request.name}!")
}

@Client("/greeting")
// @Retryable
// @CircuitBreaker
interface GreetingClient : GreetingOperations

@Fallback
class GreetingFallback : GreetingOperations {

    override fun greet(request: GreetingRequest): GreetingResponse { TODO("not implemented") }
}
