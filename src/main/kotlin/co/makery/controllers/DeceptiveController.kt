package co.makery.controllers

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController("/deceptive")
open class DeceptiveController {

    @GetMapping("/deceptive/who-am-i")
    @ResponseStatus(code = HttpStatus.OK)
    open fun whoAmI(): String = "A Spring controller"

    // http localhost:8080/deceptive/who-am-i
}

// https://bit.ly/2IdRxIW
