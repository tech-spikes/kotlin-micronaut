package co.makery.repository

import co.makery.controllers.Customer
import io.micronaut.context.annotation.Prototype
import io.reactivex.Flowable
import org.davidmoten.rx.jdbc.Database
import javax.sql.DataSource

@Prototype
class CustomerRepository constructor(dataSource: DataSource) {

    private val database = Database.fromBlocking(dataSource)

    fun fetchCustomers(): Flowable<Customer> =
        database
            .select("SELECT * FROM customers")
            .get { r -> Customer(r.getString("name"), r.getString("email"), r.getInt("age")) }
}
