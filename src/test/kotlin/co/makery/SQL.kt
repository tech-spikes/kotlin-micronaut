package co.makery

val ddl = """
    CREATE TABLE customers(
        name varchar(50) PRIMARY KEY,
        email varchar(50) NOT NULL,
        age int NOT NULL
    );
""".trimIndent()

val bootstrap = """
    INSERT INTO customers(name, email, age) values
        ('Blob', 'blob@makery.co', 30),
        ('Gyula', 'gyula@makery.co', 31);
""".trimIndent()
