package co.makery

import co.makery.controllers.CustomerController
import io.kotlintest.Spec
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.context.ApplicationContext
import io.micronaut.context.env.PropertySource
import io.micronaut.runtime.server.EmbeddedServer
import org.davidmoten.rx.jdbc.Database
import org.testcontainers.containers.PostgreSQLContainer
import javax.sql.DataSource

class KPostgreSQLContainer : PostgreSQLContainer<KPostgreSQLContainer>()

class CustomerControllerSpec : BehaviorSpec() {

    private lateinit var dbConfig: Map<String, Any>

    private val server by lazy {
        autoClose(
            ApplicationContext.run(
                EmbeddedServer::class.java,
                PropertySource.of("test", dbConfig)
            )
        )
    }

    private val db by lazy {
        val dataSource = server.applicationContext.createBean(DataSource::class.java)
        Database.fromBlocking(dataSource)
    }

    private val client by lazy { server.applicationContext.getBean(CustomerController::class.java) }

    override fun beforeSpec(spec: Spec) {
        super.beforeSpec(spec)
        val container = KPostgreSQLContainer()
        container.start()
        dbConfig = mapOf(
            "datasources.default.url" to container.jdbcUrl,
            "datasources.default.username" to container.username,
            "datasources.default.password" to container.password
        )
    }

    init {
        given("we have Blob in the database") {
            db.update(ddl).complete().blockingAwait()
            db.update(bootstrap).counts().blockingLast()
            `when`("we fetch customers") {
                val customers = client.customers().blockingIterable().toList()
                then("the result contains Blob") {
                    customers.find { it.name == "Blob" } shouldNotBe null
                }
            }
        }
    }
}
