package co.makery

import co.makery.controllers.GreetingClient
import co.makery.controllers.GreetingRequest
import co.makery.controllers.GreetingResponse
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.context.ApplicationContext
import io.micronaut.retry.exception.FallbackException
import io.micronaut.runtime.server.EmbeddedServer

class GreetingControllerSpec : BehaviorSpec() {

    private val server = autoClose(ApplicationContext.run(EmbeddedServer::class.java))
    private val client = server.applicationContext.getBean(GreetingClient::class.java)

    init {
        given("the GreetingController") {

            `when`("it receives a valid GreetingRequest") {
                val response: GreetingResponse = client.greet(GreetingRequest("Makers"))
                then("returns a GreetingResponse") {
                    response.message shouldBe "Welcome Makers!"
                }
            }

            `when`("it receives an invalid GreetingRequest") {
                val request = GreetingRequest(" ")
                then("throws HttpClientResponseException") {
                    shouldThrow<FallbackException> {
                        client.greet(request)
                    }
                }
            }
        }
    }
}
