package co.makery

import io.kotlintest.matchers.string.shouldStartWith
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import java.util.Optional

class AdvancedControllerSpec : BehaviorSpec() {

    private val server = autoClose(ApplicationContext.run(EmbeddedServer::class.java))
    private val client = autoClose(HttpClient.create(server.url))

    init {
        given("the AdvancedController") {

            `when`("it receives a low-level request to /html") {
                val request: HttpRequest<String> = HttpRequest.GET("/advanced/html")
                val response: HttpResponse<String> = client.toBlocking().exchange(request)
                then("responds with a 200") {
                    response.status shouldBe HttpStatus.OK
                    response.contentType shouldBe Optional.of(MediaType.TEXT_HTML_TYPE)
                }
            }

            `when`("it receives a simple request to /html") {
                val response: String = client.toBlocking().retrieve("/advanced/html")
                then("responds with HTML content") {
                    response shouldStartWith "<html>"
                }
            }
        }
    }
}
