import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    id("com.github.johnrengelman.shadow") version Version.Build.shadow
    id("io.gitlab.arturbosch.detekt") version Version.Tool.detekt
    id("io.spring.dependency-management") version Version.Build.dependencyManagement
    id("org.jetbrains.kotlin.jvm") version Version.kotlin
    id("org.jetbrains.kotlin.kapt") version Version.kotlin
    id("org.jetbrains.kotlin.plugin.allopen") version Version.kotlin
    id("org.springframework.boot") version "2.1.2.RELEASE"
}

version = "0.1.0"
group = "co.makery"

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

dependencyManagement {
    imports {
        mavenBom("io.micronaut:micronaut-bom:${Version.micronaut}")
    }
}

dependencies {
    compile("com.github.davidmoten:rxjava2-jdbc:${Version.rxJdbc}")
    compile("io.micronaut.configuration:micronaut-hibernate-validator")
    compile("io.micronaut.spring:micronaut-spring-web:${Version.micronautSpring}")
    compile("io.micronaut:micronaut-http-client")
    compile("io.micronaut:micronaut-http-server-netty")
    compile("io.micronaut:micronaut-runtime")
    compile("io.swagger.core.v3:swagger-annotations")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:${Version.Tool.detekt}")
    kapt("io.micronaut.configuration:micronaut-openapi")
    kapt("io.micronaut.spring:micronaut-spring-web-annotation:${Version.micronautSpring}")
    kapt("io.micronaut:micronaut-inject-java")
    kapt("io.micronaut:micronaut-validation")
    kaptTest("io.micronaut.spring:micronaut-spring-web-annotation:${Version.micronautSpring}")
    kaptTest("io.micronaut:micronaut-inject-java")
    runtime("ch.qos.logback:logback-classic:${Version.logback}")
    runtime("com.fasterxml.jackson.module:jackson-module-kotlin:${Version.jackson}")
    runtime("io.micronaut.configuration:micronaut-jdbc-hikari")
    runtime("org.postgresql:postgresql:${Version.postgres}")
    testCompile("io.kotlintest:kotlintest-runner-junit5:${Version.Test.kotlintest}")
    testCompile("io.mockk:mockk:${Version.Test.mockk}")
    testCompile("org.testcontainers:postgresql:${Version.Test.testContainers}")
    testRuntime("io.micronaut.configuration:micronaut-jdbc-hikari")
    testRuntime("org.postgresql:postgresql:${Version.postgres}")
}

val run by tasks.getting(JavaExec::class) { jvmArgs("-noverify", "-XX:TieredStopAtLevel=1", "-Xmx25M") }

val shadowJar by tasks.getting(ShadowJar::class) { mergeServiceFiles() }

val test by tasks.getting(Test::class) {
    @Suppress("UnstableApiUsage")
    useJUnitPlatform {
        systemProperties("micronaut.environments" to "test")
    }
}

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

val compileTestKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

allOpen { annotation("io.micronaut.aop.Around") }

application { mainClassName = "co.makery.Application" }

detekt {
    toolVersion = Version.Tool.detekt
    input = files("src/main/kotlin")
    config = files("default-detekt-config.yml", "detekt-config.yml")
}

tasks.getByName("build") {
    dependsOn(tasks.getByName("detekt"))
}
